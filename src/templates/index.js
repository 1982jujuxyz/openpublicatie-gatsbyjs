import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import Layout from '../components/shared/Layout'
import SEO from '../components/shared/SEO'
import { DocumentText } from 'styled-icons/typicons/DocumentText'
import Container from '../components/shared/Container'

export default class IndexPage extends React.Component {

  render() {
    
    return (
      <Layout>
        <SEO title="Home" />

        <Header>
          <DocumentText style={tw`h-16`} />
          <Title>Open Publicatie</Title>
          <Subtitle>Stroomlijn open publicatie verzoeken</Subtitle>
        </Header>
        <Section>
          <Container>
            <ColumnWrapper>
              <Column>
                <ColumnInner>Of je nu een professionele data-gebruiker bent of een geïnteresseerde die nog nooit een open publicatie verzoek heeft ingedient, wij helpen jou om het proces te stroomlijnen.</ColumnInner>
              </Column>
              <Column>
                <ColumnInner>Met Open Publicatie kun je de voortgang van jouw verzoek volgen, blijft het georganiseerd en blijf je op de hoogte van het proces.</ColumnInner>
              </Column>
              <Column>
                <ColumnInner>Tot nu toe moesten journalisten zelf door de jungle van open publicatie zwoegen. Nu kun je de hulp krijgen van de collectieve stem van de community.</ColumnInner>
              </Column>
            </ColumnWrapper>
          </Container>
        </Section>
        
        <Section style={tw`text-center`}>
          <RequestsButton to="/verzoeken">Bekijk de verzoeken</RequestsButton>
        </Section>

      </Layout>
    )
  }
  
}

const Header = styled.header`${tw`text-center p-32 bg-primary text-white`}`
const Title = styled.h1`${tw`font-bold uppercase`}`
const Subtitle = styled.h2`${tw`font-light uppercase`}`
const Section = styled.section`${tw`py-16`}`
const ColumnWrapper = styled.section`${tw`flex flex-wrap m-auto`}`
const Column = styled.div`${tw`w-full sm:w-1/3 leading-normal`}`
const ColumnInner = styled.div`${tw`p-4`}`
const RequestsButton = styled(Link)`${tw`text-center m-8 p-4 inline-block bg-blue-800 rounded text-white no-underline`}`
