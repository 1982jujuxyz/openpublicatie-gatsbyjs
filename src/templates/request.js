import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import Layout from '../components/shared/Layout'
import SEO from '../components/shared/SEO'
import Container from '../components/shared/Container'
import CommunicationsList from '../components/communications/CommunicationsList'
import DiscussionList from '../components/discussion/DiscussionList'
import FilesList from '../components/files/FilesList'
import NotificationsList from '../components/notifications/NotificationsList'
import RequestDetails from '../components/request/RequestDetails'
import RequestShare from '../components/request/RequestShare'

class RequestPage extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      activeTab: 'communications'
    }
  }

  toggleTab = (tab, e) => {
    e.preventDefault();
    this.setState({
      activeTab: tab
    })
  }

  render() {
    
    const { request } = this.props.data; 
    if (!request) { return null; }
    const organization = request.organization || {}; 

    return (
      <Layout>
        <SEO title={`${request.title } | Open Publicatie`} />
        <Helmet title={`${request.title } | Open Publicatie`} />
        <PageHeader>
          <Container>
            <h2 style={tw`font-sans font-thin uppercase`}>Verzoek tot open publicatie</h2>
            <h1>{ request.title }</h1>
            <div><span style={tw`font-bold`}>{request.user.username}</span> heeft dit verzoek ingediend bij <span style={tw`font-bold`}>{organization.title}</span>.</div>
          </Container>
        </PageHeader>

        <PageBody>
          <Container>
            <MainContentWrapper>
              <LeftSidebar style={tw`leading-loose`}>
                <RequestShare request={request} />
                <RequestDetails request={request} />
                <div style={tw`border-blue-300 border-solid border bg-blue-100 px-2 text-center text-sm rounded`}>
                  <p>Open Publicatie users can file, duplicate, track, and share public records requests like this one.</p>
                </div>
              </LeftSidebar>
              <MainContent>
                
                <TabsWrapper>
                  <TabsMenu>
                    <TabsItem active={(this.state.activeTab==='communications')}>
                      <TabsLink 
                        active={(this.state.activeTab==='communications')}
                        onClick={(e) => this.toggleTab('communications', e)}>Communicatie<TabCount>{request.communications.length}</TabCount></TabsLink>
                    </TabsItem>
                    <TabsItem active={(this.state.activeTab==='discussion')}>
                      <TabsLink 
                        active={(this.state.activeTab==='discussion')}
                        onClick={(e) => this.toggleTab('discussion', e)}>Discussie</TabsLink>
                    </TabsItem>
                    <TabsItem active={(this.state.activeTab==='files')}>
                      <TabsLink 
                        active={(this.state.activeTab==='files')}
                        onClick={(e) => this.toggleTab('files', e)}>Bestanden</TabsLink>
                    </TabsItem>
                    <TabsItem active={(this.state.activeTab==='notifications')}>
                      <TabsLink 
                        active={(this.state.activeTab==='notifications')} 
                        onClick={(e) => this.toggleTab('notifications', e)}>Notificaties<TabCount>{request.notifications.length}</TabCount></TabsLink>
                    </TabsItem>
                  </TabsMenu>
                </TabsWrapper>

                <div style={tw`p-2`}>
                  {(this.state.activeTab === 'communications') && (
                    <CommunicationsList communications={request.communications} />
                  )}
                  {(this.state.activeTab === 'discussion') && (
                    <DiscussionList discussion={request.discussion} />
                  )}
                  {(this.state.activeTab === 'files') && (
                    <FilesList files={request.files} />
                  )}
                  {(this.state.activeTab === 'notifications') && (
                    <NotificationsList notifications={request.notifications} />
                  )}
                </div>
              </MainContent>
            </MainContentWrapper>
          </Container>
        </PageBody>

        <PageFooter></PageFooter>
      </Layout>
    )
  }
}

const PageHeader = styled.header`${tw`pt-8 pb-12 text-center bg-primary text-white`}`
const PageBody = styled.section`${tw`py-8`}`
const MainContentWrapper = styled.section`${tw`flex`}`
const LeftSidebar = styled.div`${tw`w-1/3 pr-4`}`
const MainContent = styled.div`${tw`w-2/3`}`
const PageFooter = styled.footer`${tw`py-16`}`
const TabsWrapper = styled.div`${tw``}`
const TabsMenu = styled.ul`${tw`flex border-b list-none p-0 m-0`}`
const TabsItem = styled.li`${tw`mr-1`}`
const TabCount = styled.span`${tw`px-1 py-0 ml-1 bg-gray-700 text-gray-200 rounded text-sm`}`
const TabsLink = styled.a`
  ${tw`bg-white inline-block border-l border-t border-r border-solid border-gray-300 rounded-t py-2 px-4 text-blue-700 font-semibold cursor-pointer`};
  background: ${props => {
    return props.active ? '#e2e8f0' : null}
  };
  color: ${props => {
    return props.active ? '#333' : null}
  };

`

export default RequestPage;

export const pageQuery = graphql`
  query($id: String!) {
   request: requestsJson(id: { eq: $id }) {
      id
      title
      slug
      status
      date_created
      date_filed
      date_due
      user {
        username
      }
      organization {
        title
      }
      communications {
        id
        created
        subject
        body
        to_user {
          username
        }
        from_user {
          username
        }
      }
      notifications {
        date
        message
      }
    }
  }`
