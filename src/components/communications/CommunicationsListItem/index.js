import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import * as moment from 'moment'

export default class extends React.Component {  
  
  render() {
    const {communication} = this.props; 
    if (!communication) { return null; }

    return ( 
      <ItemWrapper>
        <DateWrapper>{ moment(communication.created).format('DD MMMM YYYY - HH:mm') }</DateWrapper>
        <FromWrapper>Van: { communication.from_user.username }</FromWrapper>
        <SubjectWrapper>Onderwerp: { communication.subject }</SubjectWrapper>
        <ContentWrapper dangerouslySetInnerHTML={{__html: communication.body}}></ContentWrapper>
      </ItemWrapper>
    )
  }
}

const ItemWrapper = styled.div`${tw`mb-4 border border-solid border-gray-400`}`
const DateWrapper = styled.div`${tw`bg-white p-2 text-sm`}`
const FromWrapper = styled.div`${tw`bg-gray-400 p-2 text-md font-bold`}`
const SubjectWrapper = styled.div`${tw`bg-gray-200 p-2 text-sm`}`
const ContentWrapper = styled.div`${tw`bg-white p-2`}`