import React from 'react'
import CommunicationListItem from '../CommunicationsListItem'

class CommunicationsList extends React.Component {
  
  
  render() {
    const {communications} = this.props; 
    if (!communications) { return null; }

    return ( 
      <div>
        <h3>Communicatie</h3>
        { !communications && (
          <p>Nog geen communicatie.</p>
        )}
        { communications.map(item => (
          <CommunicationListItem communication={item} key={item.id} />
        )) }
      </div>
    )
  }
}

export default CommunicationsList; 