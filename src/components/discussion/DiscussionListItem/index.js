import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'

export default class extends React.Component {
  
  
  render() {
    const {notification} = this.props; 
    if (!notification) { return null; }

    return ( 
      <ItemWrapper>
        <DateWrapper>{ notification.created }</DateWrapper>
        <FromWrapper>Van: { notification.from_user.username }</FromWrapper>
        <SubjectWrapper>Onderwerp: { notification.subject }</SubjectWrapper>
        <ContentWrapper dangerouslySetInnerHTML={{__html: notification.body}}></ContentWrapper>
      </ItemWrapper>
    )
  }
}

const ItemWrapper = styled.div`${tw`mb-4 border border-solid border-gray-400`}`
const DateWrapper = styled.div`${tw`bg-gray-400 p-2 text-md`}`
const FromWrapper = styled.div`${tw`bg-gray-400 p-2 text-md font-bold`}`
const SubjectWrapper = styled.div`${tw`bg-gray-200 p-2 text-sm`}`
const ContentWrapper = styled.div`${tw`bg-white p-4`}`
// export default NotificationsListItem; 