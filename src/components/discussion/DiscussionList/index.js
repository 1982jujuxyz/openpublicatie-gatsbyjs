import React from 'react'
import DiscussionListItem from '../DiscussionListItem'

export default class extends React.Component {
  
  
  render() {
    const {discussion} = this.props; 

    return ( 
      <div>
        <h3>Discussie</h3>
        { !discussion && (
          <p>Discussie op deze aanvaag is niet mogelijk.</p>
        )}
        { discussion && discussion.map(item => (
          <DiscussionListItem discussion={item} key={item.id} />
        )) }
      </div>
    )
  }
}
