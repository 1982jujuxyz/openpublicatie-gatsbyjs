import React from 'react'
// import styled from 'styled-components'
// import tw from 'tailwind.macro'

import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  RedditShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailIcon, 
  FacebookIcon,
  LinkedinIcon,
  RedditIcon,
  WhatsappIcon,
  TwitterIcon
} from "react-share";



export default class extends React.Component {

  render() {
    const shareUrl = `https://openpublicatie.nl/verzoek/${this.props.request.slug}`
    const quote = `${this.props.request.title} via @openpublicatie`
    return (
      <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '1rem'}}>
        <TwitterShareButton 
          url={shareUrl}
          title={quote}
        ><TwitterIcon size={32} round={true} /></TwitterShareButton>
        <LinkedinShareButton 
          url={shareUrl}
          title={quote}
        ><LinkedinIcon size={32} round={true} /></LinkedinShareButton>
        <FacebookShareButton 
          url={shareUrl}
          title={quote}
        ><FacebookIcon size={32} round={true} /></FacebookShareButton>
        <RedditShareButton 
          url={shareUrl}
          title={quote}
        ><RedditIcon size={32} round={true} /></RedditShareButton>
        <WhatsappShareButton 
          url={shareUrl}
          title={quote}
        ><WhatsappIcon size={32} round={true} /></WhatsappShareButton>
        <EmailShareButton 
          url={shareUrl} 
          title={quote}
        ><EmailIcon size={32} round={true} /></EmailShareButton>
      </div>
    )
  }
}