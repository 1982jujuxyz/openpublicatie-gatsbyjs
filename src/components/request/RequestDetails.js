import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import * as moment from 'moment'

export default class extends React.Component {

  render() {
    const { request } = this.props; 
    if (!request) { return null; }
    const organization = request.organization || {}; 

    return (
      <div>
        <Table>
          <tbody>
            <TR><TD l first>Bronhouder</TD><TD>{ organization.title }</TD></TR>
            <TR><TD l>Tracking #</TD><TD>OPNL-{ request.id }</TD></TR>
            <TR><TD l>Aangemaakt</TD><TD>{ moment(request.date_created).format('DD MMMM YYYY') }</TD></TR>
            <TR><TD l>Ingediend</TD><TD>{ moment(request.date_filed).format('DD MMMM YYYY') }</TD></TR>
            <TR><TD l last>Afgerond</TD><TD>9 november 2017</TD></TR>
          </tbody>
        </Table>

        <StatusWrapper>
          <StatusCard>{ request.status }</StatusCard>
        </StatusWrapper>

        <CountWrapper>
          Aantal dagen sinds indiening:
          <CountItem>101</CountItem>
        </CountWrapper>
      </div>
    )
  }
}

const Table = styled.table`${tw`text-sm mb-4  w-full`}`
const TR = styled.tr`${tw``}`
const TD = styled.td`
  ${tw`p-2 text-gray-800`}; 
  border-top: 1px solid #efefef;
  font-weight: ${props => {
    return props.l ? 'bold' : 'normal'
  }};`
const StatusWrapper = styled.div`${tw`text-center`}`
const StatusCard = styled.span`${tw`p-2 bg-gray-100 border border-solid border-gray-600 rounded`}`
const CountWrapper = styled.div`${tw`py-16 text-center`}`
const CountItem = styled.div`${tw`text-3xl font-bold`}`