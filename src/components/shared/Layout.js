import React from 'react'
import PropTypes from 'prop-types'
import { useStaticQuery, graphql } from 'gatsby'
import Header from './Header'
import Footer from './Footer'
import { createGlobalStyle } from 'styled-components'
import Normalize from 'react-normalize'
import styled from 'styled-components'
import tw from 'tailwind.macro'

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    font-family: 'Source Sans Pro', sans-serif;
    h1, h2, h3,h4, h5, h6 {
      font-family: 'Bungee', sans-serif;
    }

    a {
      color: #666;
      &:hover {
        color: #000;
      }
    }
  }
`

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <React.Fragment>
      <Normalize />
      <GlobalStyle />
      <Header siteTitle={data.site.siteMetadata.title} />
      <Container>
        <main>{children}</main>
      </Container>
      <Footer />
    </React.Fragment>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

const Container = styled.div`${tw``}`
export default Layout
