import React from 'react'
import { Link } from 'gatsby'
import tw from 'tailwind.macro'
import styled from 'styled-components'
import { DocumentText } from 'styled-icons/typicons/DocumentText'
import Container from './Container'

export default (props) => {

  return (
    <PageFooter>
      <Container>
        <Wrapper>
          <First>
            <DocumentText style={tw`h-4 mr-1 text-gray-600`} />
            Open Publicatie is een project dat jou de mogelijkheden geeft om onze overheid transparant te maken.<br />
          </First>

          <Second>
            <Menu>
              <MenuItem><MenuItemLink to="/about">About</MenuItemLink></MenuItem>
              {/* <MenuItem>FAQ</MenuItem> */}
              <MenuItem><MenuItemLink to="/contact">Contact</MenuItemLink></MenuItem>
              {/* <MenuItem>Privacybeleid</MenuItem> */}
            </Menu>
          </Second>
        </Wrapper>
        <SubFooter>© 2016-2020 openpublicatie.nl, van <a href="https://urbanlink.nl" target="_blank" rel="noopener noreferrer">urbanlink.nl</a></SubFooter>
      </Container>
    </PageFooter>
  )
}

const PageFooter = styled.footer`${ tw`px-4 py-12 bg-gray-800 text-gray-600 flex text-sm` }`
const Wrapper =styled.div`${tw`flex flex-wrap`}`
const First = styled.div`${tw`w-1/3 leading-loose`}`
const Second = styled.div`${tw`w-2/3 text-right`}`
const Menu = styled.ul`${tw`flex m-0 leading-loose`}; justify-content: flex-end;`
const MenuItem = styled.li`${tw`ml-2 list-none text-gray-600`}`
const MenuItemLink = styled(Link)`${tw`text-gray-600 hover:text-gray-400 no-underline`}`
const SubFooter = styled.div`${tw`text-sm text-gray-700 pt-8`};
  a {
    ${tw`text-gray-700`}
  }
`