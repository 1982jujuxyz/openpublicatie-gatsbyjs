import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import * as moment from 'moment'

export default class extends React.Component {
  
  render() {
    const {notification} = this.props; 
    if (!notification) { return null; }

    return ( 
      <Wrapper>
        <strong>{ moment(notification.date).format('D MMMM YYYY') }</strong><br />{ notification.message }
      </Wrapper>
    )
  }
}

const Wrapper = styled.div`${tw`mb-4`}`
