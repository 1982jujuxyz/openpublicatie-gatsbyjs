import React from 'react'
import NotificationListItem from '../NotificationsListItem'

class NotificationsList extends React.Component {
  
  render() {
    const {notifications} = this.props; 

    return ( 
      <div>
        <h3>Notificaties</h3>
        { !notifications && (
          <p>Nog geen notificaties.</p>
        )}
        { notifications && notifications.map(item => (
          <NotificationListItem notification={item} key={item.id} />
        )) }
      </div>
    )
  }
}

export default NotificationsList; 