import React from 'react'
import FilesListItem from '../FilesListItem'

export default class extends React.Component {
  
  
  render() {
    const { files } = this.props;

    return ( 
      <div>
        <h3>Bestanden</h3>
        { !files && (
          <p>Er zijn nog geen bestanden beschikbaar voor deze aanvraag.</p>
        )}
        { files && files.map(item => (
          <FilesListItem file={item} key={item.id} />
        )) }
      </div>
    )
  }
}
