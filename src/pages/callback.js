import React from 'react'
import { navigate } from 'gatsby'
import styled from 'styled-components'
import { handleAuthentication } from '../utils/auth.service'
import Layout from '../components/shared/Layout'

export default () => {

  handleAuthentication(() => navigate('/'));

  return (
    <Layout>
      <MsgStyle>Logging you in ...</MsgStyle>
    </Layout>
  );
};

const MsgStyle = styled.div`
  text-align: center;
  padding: 50px;
`
