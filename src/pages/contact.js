import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import SEO from '../components/shared/SEO'
import Layout from '../components/shared/Layout'
import Container from '../components/shared/Container'

export default () => {

  return (
    <Layout>

      <SEO title='Contact' />
      
      <Container>
        <PageHeader>
          <h1>Contact</h1>
        </PageHeader>

        <PageBody>
          <p>Open publicatie is een project van <a href="https://urbanlink.nl">urbanlink.nl</a>. Neem contact op per mail: <a href="mailto:arn@urbanlink.nl">arn@urbanlink.nl</a></p>
        </PageBody>
        <PageFooter></PageFooter>
      </Container>
    </Layout>
  );
};

const PageHeader = styled.header`${tw`text-center py-16`}`
const PageBody = styled.section`${tw`py-4 text-center`}`
const PageFooter = styled.footer`${tw`py-8`}`