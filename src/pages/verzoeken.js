import React from 'react'
import { graphql, Link } from 'gatsby'
import tw from 'tailwind.macro'
import styled from 'styled-components'
import * as moment from 'moment'
import Layout from '../components/shared/Layout'
import SEO from '../components/shared/SEO'
import Container from '../components/shared/Container'

class RequestsPage extends React.Component {

  render() {
    
    const {requests } = this.props.data; 
    if (!requests) { return null; }

    return (
      <Layout>
        <SEO title="Verzoeken" />

        <Header>
          <Container>
            <Title>Verzoeken</Title>
            <Subtitle>We beheren alle open publicatie verzoeken die via ons systeem zijn ingediend, zodat deze publiek en inzichtelijk worden.</Subtitle>
          </Container>
        </Header>

        <Container>
          <h3 style={tw`pt-8`}>Verzoeken</h3>
          { requests.edges.map(({node}) => (
            <ItemCard to={`/verzoek/${node.slug}`} key={node.id}>
              <h3>{ node.title }</h3>
              { moment(node.date_created).format('DD MMMM YYYY') }
            </ItemCard>
          ))}
        </Container>
      </Layout>
    );
  }
}

export default RequestsPage;

const Header = styled.header`${tw`text-center p-12 bg-blue-800 text-white`}`
const Title = styled.h1`${tw`font-bold uppercase`}`
const Subtitle = styled.h2`${tw`font-light font-sans leading-normal uppercase`}`
const ItemCard = styled(Link)`${tw`block text-black no-underline bg-gray-100 border border-solid border-gray-300 hover:bg-gray-300 p-16 my-8`}`

export const pageQuery = graphql`
  query {
    requests: allRequestsJson {
      edges {
        node {
          id,
          title,
          slug,
          date_created,
          user {
            id,
            username
          }
        }
      }
    }
  }
`;