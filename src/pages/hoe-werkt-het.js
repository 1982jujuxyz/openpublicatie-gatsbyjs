import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import SEO from '../components/shared/SEO'
import Layout from '../components/shared/Layout'
import Container from '../components/shared/Container'

export default () => {

  return (
    <Layout>

      <SEO title='Hoe werkt Open Publicatie' />
      
      <Container>
        <PageHeader>
          <h1>Hoe werkt Open Publicatie?</h1>
          <h2 style={tw`font-sans leading-loose mt-16 font-thin text-3xl `}>Open Publicatie ondersteunt je in het voorbereiden, indienen en volgen van aanvragen voor publieke documenten</h2>
        </PageHeader>

        <PageBody style={tw`leading-loose`}>
          <div style={tw`text-xl leading-loose text-justify`}>
            <h3>What is Open Publicatie?</h3>
            <p>Open Publicatie is een open-source platform die het mogelijk maakt om verzoeken voor open publicatie te schrijven, in te dienen bij (publieke) instellingen en te volgen via het platform. <br />
            Deze site helpt haar gebruikers in de toegang naar documenten en data die (onder andere) valt onder de Wet Openbaarheid Bestuur (WOB). Dit zorgt ervoor dat burgers, journalisten en belangenorgansities informatie kunnen ontvangen op een transparante manier, die van vitaal belang is voor het werken van hun overheid.</p>
            <p>Deze site is ontwikkeld door <a href="https://urbanlink.nl" target="_blank" rel="noopener noreferrer">urbanlink</a>, en is afhankelijk van donaties, investeringen, lidmaatschappen en inhoudelijke ondersteuning voor verdere ontwikkeling.<br />
            Na een eerste pilot in 2016 op basis van <a target="_blank" href="http://gitlab.com/urbanlink/openpublicatie-gatsbyjs" rel="noopener noreferrer">Wordpress</a> is het verder ontwikkeld tot Proof of Concept op basis van <a target="_blank" href="http://gitlab.com/urbanlink/openpublicatie-gatsbyjs" rel="noopener noreferrer">NodeJS</a> en <a target="_blank" href="http://gitlab.com/urbanlink/openpublicatie-gatsbyjs" rel="noopener noreferrer">GatsbyJS</a>. We zijn op dit moment op zoek naar ondersteuning vanuit onderzoeksjournalistiek en onderwijs/onderzoek.</p>
          </div>
          <hr style={tw`my-16`}/>
          <h2 style={tw`my-8`}>Veelgestelde Vragen</h2>
          
          <h3>Wat kan ik doen met Open Publicatie?</h3>
          <ul>
            <li>Een verzoek voorbereiden onder de Wet Openbaarheid Bestuur of met toepasselijke wetgeving voor een bepaalde overheidsinstantie</li>
            <li>Zoek eenvoudig het juiste contact</li>
            <li>Volg en update de huidige status van je aanvraag</li>
            <li>Deel een verzoek met andere Open Publicatie-gebruikers</li>
            <li>Deel je WOB-ervaring met andere gebruikers</li>
          </ul>
          <p>Kijk op de pagina <Link to="voor-wie">Voor Wie?</Link> voor meer informatie voor verschillende doelgroepen.</p>

          <h3>Wat is WOB (Wet Openbaarheid Bestuur?</h3>
          <p><a href="https://nl.wikipedia.org/wiki/Wet_openbaarheid_van_bestuur" target="_blank" rel="noopener noreferrer" >[Wikipedia]</a> De Wet openbaarheid van bestuur (Wob) uit 1991 regelt de openbaarheid van bestuur door openbaarmaking van informatie door de Nederlandse overheid. Hergebruik van openbare informatie was oorspronkelijk onderdeel van de wet maar is in 2015 afgesplitst naar de Wet hergebruik van overheidsinformatie.</p>
          <p>De wet garandeert eenieder de mogelijkheid om informatie over een bestuurlijke aangelegenheid bij een bestuursorgaan (bijvoorbeeld een ministerie, provincie of gemeente) op te vragen. De Wob is geen documentenwet zoals de openbaarheidsregels bij de Europese Unie, van de VS en van bijvoorbeeld Zweden, maar een informatiewet. Het gaat om de informatie die reeds vastgelegd is, ongeacht de gegevensdrager. Zo kan het ook om informatie gaan op een USB-stick of harde schijf, om een foto, aantekeningen op een gele memosticker of een SMS-bericht op de zakelijke telefoon van een ambtenaar. De overheid heeft het laatste woord over de vorm van de gegevensverstrekking; dat kan ook mondeling zijn.</p>
          
          <h3>Hoe verdienen jullie hier geld aan?</h3>
          <p>De basisfunctionaliteiten van deze website zijn kostenloos omdat we vinden dat informatie toegankelijk moet zijn voor iedereen en de overheid transparant moet zijn. Om de website (verder) te kunnen ontwikkelen vragen we een bijdrage voor extra functionaliteiten.</p>
          
          <h3>Hoe kan ik bijdragen?</h3>
          <p>Gebruik het platform en laat ons weten hoe we Open Publicatie kunnen verbeteren door ons een e-mail te sturen. Ben je ontwikkelaar? Je kunt bijdragen door de code op <a target="_blank" href="http://gitlab.com/urbanlink/openpublicatie-gatsbyjs" rel="noopener noreferrer">GitLab</a> te verbeteren. </p>
          
          <h3>Waarom is Open Publicatie anders dan anderen?</h3>
          <p>Open Publicatie is een platform waarmee gebruikers aanvragen kunnen delen en een open database waarmee mensen kunnen samenwerken en tegelijkertijd een robuuste kennisbron over WOB-aanvragen kunnen creëren.</p>
          
          <h3>Kan ik een verzoek indienen zonder te registreren?</h3>
          <p>Nee, je moet een account hebben om verzoeken in te dienen. Je kan echter verzoeken delen met anderen die geen account hebben.</p>
          
          <h3>Zouden jullie mijn gegevens verkopen?</h3>
          <p>Absoluut niet. We respecteren je privacy en verzamelen alleen de nodige informatie om een ​​WOB-aanvraag te genereren. Af en toe maken we rapporten over hoe het systeem werkt en wie het gebruikt, maar er zal nooit identificerende informatie worden gepubliceerd.</p>
          
          <h3>Hoe weet ik of mijn verzoek is ingediend?</h3>
          <p>Je aanvraag wordt via de website verstuurd naar de organisatie. De organisatie kan antwoorden door te reageren per email die door Open Publicatie wordt verwerkt, of door in te loggen via hun organisatie-account op Open Publicatie. De communicatie verloopt via de website en is op deze manier ook inzichtelijk.</p>
          
          <h3>Hoe hou ik de voortgang van mijn aanvraag bij?</h3>
          <p>Je kunt je verzoeken volgen door je aan te melden bij Open Publicatie. Op je dashboard kun je nieuwe mededelingen, aantekeningen van gesprekken toevoegen en de algemene status van je aanvraag bijwerken.</p>
          
          <h3>Hoe kom ik in contact?</h3>
          <p>Email <a href="mailto:info@openpublicatie.nl">info@openpublicatie.nl</a>.</p>
        </PageBody>
        <PageFooter></PageFooter>
      </Container>
    </Layout>
  );
};

const PageHeader = styled.header`${tw`text-center py-16`}`
const PageBody = styled.section`${tw`py-4`}`
const PageFooter = styled.footer`${tw`py-8`}`