import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import SEO from '../components/shared/SEO'
import Layout from '../components/shared/Layout'
import Container from '../components/shared/Container'

export default () => {

  return (
    <Layout>

      <SEO title='Voor wie is Open Publicatie' />
      
      <Container>
        <PageHeader>
          <h1>Voor wie is Open Publicatie</h1>
          <h2 style={tw`font-sans leading-loose mt-16 font-thin text-3xl `}>Open Publicatie is van meerwaarde voor zowel de aanvrager als de bronhouder</h2>
        </PageHeader>

        <PageBody>
          <h3>(Onderzoeks) journalisten</h3>
          <ul>
            <li>Beheer je verzoeken op 1 locatie</li>
            <li>Krijg inzicht in de status van de aanvraag</li>
            <li>Integreer de aanvragen in je eigen website </li>
            <li>Gebruik de website om de gegevens te publiceren</li>
          </ul>
          
          <h3>Bewoners</h3>
          <ul>
            <li>Krijg ondersteuning in het proces van aanvragen voor open publicatie</li>
            <li>Kom in contact met de juiste contactpersoon voor je aanvraag</li>
            <li>Krijg inzicht in de status van de aanvraag</li>
            <li>Integreer de aanvragen in je eigen website </li>
            <li>Gebruik de website om de gegevens te publiceren</li>
          </ul>
          
          <h3>Belangenorganisaties</h3>
          <ul>
            <li>Krijg ondersteuning in het proces van aanvragen voor open publicatie</li>
            <li>Kom in contact met de juiste contactpersoon voor je aanvraag</li>
            <li>Beheer je verzoeken op 1 locatie</li>
            <li>Krijg inzicht in de status van de aanvraag</li>
            <li>Integreer de aanvragen in je eigen website </li>
            <li>Gebruik de website om de gegevens te publiceren</li>
          </ul>

          <h3>Bronhouders</h3>
          <ul>
            <li>Beheer aanvragen via 1 portaal</li>
            <li>Krijg direct inzicht in de status van alle aanvragen</li>
            <li>Integreer de aanvragen in je eigen website</li>
          </ul>
        </PageBody>
        <PageFooter></PageFooter>
      </Container>
    </Layout>
  );
};

const PageHeader = styled.header`${tw`text-center py-16`}`
const PageBody = styled.section`${tw`py-4`}`
const PageFooter = styled.footer`${tw`py-8`}`