import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import SEO from '../components/shared/SEO'
import Layout from '../components/shared/Layout'
import Container from '../components/shared/Container'

export default () => {

  return (
    <Layout>

      <SEO title='About' />
      
      <Container>
        <PageHeader>
          <h1>Over Open Publicatie</h1>
        </PageHeader>

        <PageBody>
          <p>Open Publicatie is een project van <a href="https://urbanlink.nl">urbanlink.nl</a>, een ondersteunend nieuws platform die journalisten, onderzoekers, activisten, belangenorganisaties en bewoners de mogelijkheid biedt om documenten op te vragen, te analyseren en te delen. Dit maakt de overheid en samenleving meer transparant en de democratie sterker en beter geinformeerd.</p>

          <p>Deze site biedt een bibliotheek van alle documenten en data van overheid en organisaties, informatie over hoe je een open publicatie verzoek indiend, en hulpmiddelen om het verzoek-proces eenvoudiger te maken. Daarnaast maakt het netwerk van mensen rondom Open Publicatie gebruik van de documentenen tools om onderzoek en analyse uit te voeren. </p>

          <p>Open Publicatie is ontstaan vanuit het idee om WOB verzoeken meer inzichtelijk te maken. Uit eigen ervaring bleek dat dit proces nog veel onduidelijkheden opriep een een sfeer van geheimzinnigheid had. En dat terwijl het een relatief eenvoudig proces is dat is gericht op transparantie en accountability. <br />
          Dit project is gestart onder de naam <em>nonstopwobshop</em>, en daarna doorgegroeid naar <strong>Open Publicatie</strong>. </p>

          <p>De gegevens die op Open Publicatie staan zijn kostenloos beschikbaar. Voor het indienen van verzoeken en het reageren op verzoeken is een account nodig op Open Publicatie. In de huidige alpha fase is een account kostenloos. In de toekomst zullen er verschillende pricing plans zijn. In deze prijs zit advies, controle, follow up en hosting verwerkt.</p>
        </PageBody>
        <PageFooter></PageFooter>
      </Container>
    </Layout>
  );
};

const PageHeader = styled.header`${tw`text-center py-16`}`
const PageBody = styled.section`${tw`py-4`}`
const PageFooter = styled.footer`${tw`py-8`}`